vgrabbj (0.9.9-4) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/watch: Use https protocol

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on debhelper.
  * Bump debhelper dependency to >= 10, since that's what is used in
    debian/compat.
  * Bump debhelper from old 10 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Fix day-of-week for changelog entry 0.8.3-1.

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 10:12:49 +0200

vgrabbj (0.9.9-3) unstable; urgency=medium

  * move packaging to https://salsa.debian.org/debian/vgrabbj

 -- Ludovic Rousseau <rousseau@debian.org>  Sat, 24 Mar 2018 16:00:14 +0100

vgrabbj (0.9.9-2) unstable; urgency=medium

  * Migrate the Debian packging from SVN to GIT
  * Use debhelper compat level 10 (was 9)

 -- Ludovic Rousseau <rousseau@debian.org>  Fri, 22 Dec 2017 13:24:05 +0100

vgrabbj (0.9.9-1) unstable; urgency=medium

  * New upstream release
  * debian/patches/kFreeBSD: patch removed since it is included upstream
  * Fix "Replace ftplib-dev build dependency by libftp-dev" by doing the
    suggested change (Closes: #832522)
  * debian/rules: enable all hardening
  * debian/control:
    . use https for vcs-browser URL
    . Standards-Version: 3.9.4 -> 3.9.8, no change needed

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 03 Aug 2016 18:56:02 +0200

vgrabbj (0.9.8-3) unstable; urgency=low

  * Fix "Please change from libpng3-dev to libpng-dev in Build-Depends"
    (Closes: #731729)

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 11 Dec 2013 13:09:01 +0100

vgrabbj (0.9.8-2) unstable; urgency=low

  * debian/patches/kFreeBSD: Fix compilation on GNU/kFreeBSD

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 13 Oct 2013 20:31:16 +0200

vgrabbj (0.9.8-1) unstable; urgency=low

  * New upstream release
  * debian/watch: add uscan watch file

 -- Ludovic Rousseau <rousseau@debian.org>  Sun, 13 Oct 2013 20:17:28 +0200

vgrabbj (0.9.7-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    . change debian maintainer
    . remove Build-Depends: autotools-dev,
    . Standards-Version: 3.9.2.0 -> 3.9.4, no change needed
    . update Homepage. The project is now at SourceForge.net
    . add Vcs-Svn: and Vcs-Browser: fields
  * debian/rules: use a minimal file
  * debian/compat: use level 9
  * debian/examples: add upstream examples
  * Fix "Doesn't use /etc/vgrabbj.conf but /usr/etc/.." fixed upstream
    (Closes: #597620)

 -- Ludovic Rousseau <rousseau@debian.org>  Wed, 04 Sep 2013 17:48:49 +0200

vgrabbj (0.9.6-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: make[1]: *** No targets specified and no makefile found.
    Stop.": fix target dependencies in debian/rules.
    (Closes: #666341)

 -- gregor herrmann <gregoa@debian.org>  Tue, 01 May 2012 16:33:05 +0200

vgrabbj (0.9.6-5) unstable; urgency=low

  * Add an error message when a unsupported size is requested, if
    debug level is high enough (-D 5 or higher).
  * Build-Depends on libjpeg-dev instead of libjpeg62-dev.
    (Closes: #633439)
  * Fix typo in the short description. (Closes: #631330)

 -- Michael Janssen <jamuraa@debian.org>  Sat, 16 Jul 2011 13:02:18 -0500

vgrabbj (0.9.6-4) unstable; urgency=low

  * Ack NMU. (Closes: #494935)
  * Switch to libv4l-videodev.h to fix FTBFS. (Closes: #621965)
  * Update rules to include build-arch and build-indep
  * Bump Standards Version to 3.9.2.0
  * Fix spelling error in manpage

 -- Michael Janssen <jamuraa@debian.org>  Wed, 22 Jun 2011 00:06:34 -0500

vgrabbj (0.9.6-3.2) unstable; urgency=low

  * Non-maintainer upload.
  * Modify to use libv4l1 for compatibility with current camera drivers
    (Closes: #569092)
  * Initialise all configuration variables in init_defaults()
    (Closes: #494939)
  * Bump Standards Version to 3.9.1.0
    - Fix filename of full GPLv2 text
  * Add explicit copyright line to copyright file
  * Use debhelper 7
  * Add ${misc:Depends} to vgrabbj dependencies
  * Remove config.sub and config.guess in 'clean' rule
  * Explicitly check for missing Makefile in 'clean' rule

 -- Ben Hutchings <ben@decadent.org.uk>  Thu, 29 Jul 2010 17:01:52 +0100

vgrabbj (0.9.6-3.1) unstable; urgency=low

  * Non-maintainer upload to solve RC bug.
  * Drop dependency on freetype1 by disabling the support for inserting
    timestamp text in the images (Closes: #432195).  With this change the
    need for fonts is reomved and thus it drop the suggests on
    msttcorefonts (Closes: #490033).
  * Moved homepage URL from description to the new header.
  * Remove /usr/sbin/ from dirs, as the directory is empty.

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 13 Aug 2008 09:27:12 +0200

vgrabbj (0.9.6-3) unstable; urgency=low

  * Fix off-by-one error in v_writer.c (Closes: #396987)
  * Update FSF address in debian/copyright
  * Fix bugs in manpage
  * Bump Standards Version to 3.7.2.0

 -- Michael Janssen <jamuraa@debian.org>  Tue,  1 Aug 2006 21:32:02 -0500

vgrabbj (0.9.6-2) unstable; urgency=low

  * Include an example config file and script for vgrabbj usage in
    'tricky' situations. (Closes: #129190)
  * Changes to come to Standards-Version 3.6.2.0.
  * Fix typo in manpage vgrabbj.conf.5.gz (minus as hyphen).
  * Update debian/copyright with new URL.
  * Include homepage in long description.
  * Update config.sub and config.guess automatically.

 -- Michael Janssen <jamuraa@debian.org>  Mon,  8 Aug 2005 10:41:12 -0500

vgrabbj (0.9.6-1) unstable; urgency=low

  * New upstream release (Closes: #262060)
    - Fixes typo in the usage (Closes: #190657)
  * Fix FTBFS on gcc4 (Closes: #297944)

 -- Michael Janssen <jamuraa@debian.org>  Wed, 27 Jul 2005 19:51:40 -0500

vgrabbj (0.9.3-1) unstable; urgency=low

  * New upstream release
    - boolean command line option fix (Closes: #157236)
  * Use libpng3 instead of libpng2

 -- Michael Janssen <jamuraa@base0.net>  Tue, 15 Oct 2002 17:48:14 -0500

vgrabbj (0.9.1-1) unstable; urgency=low

  * New upstream release

 -- Michael Janssen <jamuraa@base0.net>  Tue, 11 Jun 2002 18:04:31 -0500

vgrabbj (0.8.7-1) unstable; urgency=low

  * New upstream release

 -- Michael Janssen <jamuraa@debian.org>  Sat,  9 Feb 2002 13:08:14 -0600

vgrabbj (0.8.4-1) unstable; urgency=low

  * New upstream release

 -- Michael Janssen <jamuraa@debian.org>  Fri,  8 Feb 2002 13:49:56 -0600

vgrabbj (0.8.3-1) unstable; urgency=low

  * New upstream release
  * Change in v_writer.c to delete temporary files after we're done with
    them. (Closes: #127722)
  * Change v_config.c and vgrabbj.c so that we don't check files until
    we know which files we are going to open. (Closes: #127659)

 -- Michael Janssen <jamuraa@debian.org>  Tue, 05 Feb 2002 01:06:23 -0600

vgrabbj (0.8.2-1) unstable; urgency=low

  * New upstream release
  * Update config.h halfway through the build process so we look for
    the config file in the right spot. (Closes: #126484)

 -- Michael Janssen <jamuraa@debian.org>  Tue,  1 Jan 2002 23:04:59 -0600

vgrabbj (0.8.1-2) unstable; urgency=low

  * Change .SH VGRABBJ to .SH NAME in manpage (Closes: #119525)

 -- Michael Janssen <jamuraa@debian.org>  Tue, 13 Nov 2001 23:46:45 -0600

vgrabbj (0.8.1-1) unstable; urgency=low

  * New upstream release

 -- Michael Janssen <jamuraa@debian.org>  Mon, 12 Nov 2001 22:13:20 -0600

vgrabbj (0.7.4-1) unstable; urgency=low

  * New upstream release
  * Will compile on non-i386 now.

 -- Michael Janssen <jamuraa@debian.org>  Mon, 30 Jul 2001 21:17:16 -0400

vgrabbj (0.7.3-2) unstable; urgency=low

  * Doesn't compile on non-i386 architectures anymore (Closes: Bug#105738)

 -- Michael Janssen <jamuraa@debian.org>  Wed, 18 Jul 2001 09:31:58 -0400

vgrabbj (0.7.3-1) unstable; urgency=low

  * New upstream release

 -- Michael Janssen <jamuraa@debian.org>  Mon, 16 Jul 2001 16:34:51 -0400

vgrabbj (0.6.4-1) unstable; urgency=low

  * New upstream release
  * Fixed Build-Depends, removing zlib1g-dev because libpng2-dev depends on it,
    and changed freetype2-dev to libttf-dev.
  * Reworded long description to be less weird.

 -- Michael Janssen <jamuraa@debian.org>  Wed,  4 Jul 2001 21:47:16 -0400

vgrabbj (0.6.3-1) unstable; urgency=low

  * New upstream release.

 -- Michael Janssen <jamuraa@debian.org>  Tue, 27 Mar 2001 22:26:55 -0600

vgrabbj (0.6.1-1) unstable; urgency=low

  * Removed depreciated dh_testversion
  * Added DEB_BUILD_OPTIONS to debian/rules
  * Oops - forgot freetype2-dev in Build-Depends
  * Fixed Build-Depends. (I think)
  * Initial Release. (Closes: Bug#83986)
  * My first Debian package. (Yay!)

 -- Michael Janssen <jamuraa@debian.org>  Thu, 22 Mar 2001 02:06:18 -0600
